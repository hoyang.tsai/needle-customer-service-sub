const HtmlWebpackPlugin = require('html-webpack-plugin');
const { ModuleFederationPlugin } = require('webpack').container;
const path = require('path');

const deps = require('./package.json').dependencies;

module.exports = {
  entry: './src/index',
  mode: 'development',
  devServer: {
    static: path.join(__dirname, 'dist'),
    port: 3001,
    historyApiFallback: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    }
  },
  output: {
    filename: 'index.js',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: ['@babel/preset-env'],
          plugins: [
            '@babel/plugin-transform-runtime',
            '@babel/plugin-proposal-export-default-from',
            '@babel/plugin-proposal-export-namespace-from'
          ]
        }
      },
    ],
  },
  plugins: [
    // To learn more about the usage of this plugin, please visit https://webpack.js.org/plugins/module-federation-plugin/
    new ModuleFederationPlugin({
      name: 'app2',
      filename: 'remoteEntry.js',
      exposes: {
        './App': './src/App.js',
      },
      shared: {
        // ...deps,
        // react: { singleton: true, eager: true, requiredVersion: deps['react']  },
        // 'react-dom': { singleton: true, eager: true, requiredVersion: deps['react-dom']  },
        // redux: { singleton: true, eager: true, requiredVersion: deps['redux'] },
        // 'react-redux': { singleton: true, eager: true, requiredVersion: deps['react-redux'] },
        // 'react-router': { singleton: true, eager: true, requiredVersion: deps['react-router'] },
        // 'react-router-dom': { singleton: true, eager: true, requiredVersion: deps['react-router-dom'] },
        // 'react-select': { singleton: true, eager: true, requiredVersion: deps['react-select'] },
        // 'redux-thunk': { singleton: true, eager: true, requiredVersion: deps['redux-thunk'] },
        // 'connected-react-router': { singleton: true, eager: true, requiredVersion: deps['connected-react-router'] },
        // 'jss': { singleton: true, eager: true, requiredVersion: deps['jss'] },
        // 'react-jss': { singleton: true, eager: true, requiredVersion: deps['react-jss'] },
        // 'history': { singleton: true, eager: true, requiredVersion: deps['history'] },
        // 'react-loadable': { singleton: true, eager: true, requiredVersion: deps['react-loadable'] },
        // 'react-window': { singleton: true, eager: true, requiredVersion: deps['react-window'] },
        // 'styled-components': { singleton: true, eager: true, requiredVersion: deps['styled-components'] },
        // 'styled-normalize': { singleton: true, eager: true, requiredVersion: deps['styled-normalize'] },
        // '@material-ui/core': { singleton: true, eager: true, requiredVersion: deps['@material-ui/core'] },
        // '@material-ui/icons': { singleton: true, eager: true, requiredVersion: deps['@material-ui/icons'] },
        // 'recompose': { singleton: true, eager: true, requiredVersion: deps['recompose'] },
        // 'redux-promise-middleware': { singleton: true, eager: true, requiredVersion: deps['redux-promise-middleware'] },
        // 'redux-devtools-extension': { singleton: true, eager: true, requiredVersion: deps['redux-devtools-extension'] },
      },
    }),
    new HtmlWebpackPlugin({
      template: './public/index.html'
    }),
  ]
};